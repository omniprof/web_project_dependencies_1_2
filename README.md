Parent pom.xml Project Version 1.2

This project is the new updated parent pom.xml file for all sample code in the Java/Jakarta EE courses that I give. It is based on Jakarta EE 8.0, Arquillian, EclipseLink, and Selenium along with other dependencies to support them. To use this parent you must first clone this project to your computer. Next, you must use the Maven command mvn install on the project. If you are using NetBeans, that all my students use, right click on the project and select Run Maven. Give it a goal of install:install. This project's pom.xml will be placed in your local repository for use in all of the other projects you clone for my courses.

This version of the parent pom.xml is not backwards compatible with projects that need version 1.1. Clone my web_project_dependencies.git project for that.
